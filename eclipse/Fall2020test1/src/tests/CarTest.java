	//Momshad Hussain 1837713
	
	package tests;
	
	import static org.junit.jupiter.api.Assertions.*;
	
	import org.junit.jupiter.api.Test;
	
	class CarTest {
	
		@Test
		public void testCar() {
			try {
			Car car = new Car(-29);
			fail("The car constructor was supposed to throw an error but did not");
			
			}
			catch(IllegalArgumentException e)
			{
				
			}
			
			catch(Exception e )
			{
				 fail("Speed must not be neagtive");
			}
		}
		
		
		@Test
		public void testGetSpeed()
		{
			Car car = new Car(20);
			assertTrue(car.getSpeed()==20);
		}
		
		@Test
		public void testLocation()
		{
			Car car = new Car(100);
			assertTrue(car.getLocation()==50);
		}
		
		@Test
		public void moveRight()
		{
			Car car = new Car(20);
			int result = car.getLocation();
			result = car.getLocation() + car.getSpeed();
			assertTrue(result==70);
			
		}
		
		@Test
		public void moveleft()
		{
			Car car = new Car(20);
			int result = car.getLocation();
			result = car.getLocation() - car.getSpeed();
			assertTrue(result==30);
			
		}
		
		@Test
		public void testAccelerate()
		{
			Car car = new Car(20);
			int result = (car.getSpeed()) + 1;
			assertTrue(result == 21);
			
		}
		
		@Test
		public void testStop()
		{
			Car car = new Car(20);
			int result = (car.getSpeed()) - car.getSpeed();
			assertTrue(result == 0);
		}	
	}

