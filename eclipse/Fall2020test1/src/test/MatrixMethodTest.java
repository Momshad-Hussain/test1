//Momshad Hussain 1837713
package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTest {

	
	//creating a test method to see if the array is duplicating as it should
	@Test
	public void  testDuplicate() {
		
		int [][] array = new int [][]  {{1,2,3} , {4,5,6}};
		int [][] expected = new int [][]  {{1,1,2,2,3,3} , {4,4,5,5,6,6}};
		int [][]result = duplicate(array);
		assertEquals(expected, result);
		
	}
}
